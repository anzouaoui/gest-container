﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace Gestion_Container
{
    public partial class InspectionPrevus : Form
    {
        private List<Etat> _collectionEtat = new List<Etat>();
        public InspectionPrevus()
        {
            InitializeComponent();
        }

        private void InspectionPrevus_Load(object sender, EventArgs e)
        {
            recupererEtat();
        }

        private void recupererEtat()
        {
            _collectionEtat = Etat.FetchAll();
            foreach (Etat etatCourant in _collectionEtat)
            {
                comboBoxEtat.Items.Add(etatCourant.LibelleEtat);
            }
        }
    }
}
