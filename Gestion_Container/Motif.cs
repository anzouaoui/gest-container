﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Container
{
    class Motif
    {
            private string _codeMotif;

            public string CodeMotif
            {
                get { return _codeMotif; }
                set { _codeMotif = value; }
            }
            private string _libelleMotif;

            public string LibelleMotif
            {
                get { return _libelleMotif; }
                set { _libelleMotif = value; }
            }


            public Motif(string codeMotif, string LibelleMotif)
            {
                _codeMotif = codeMotif;
                _libelleMotif = LibelleMotif;
            }
        
    }
}

