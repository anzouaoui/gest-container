﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace Gestion_Container
{
    /// <summary>
    /// Classe Déclaration: Représentation d'un probleme
    /// </summary>
    class Probleme
    {
        #region Modèle Objet
        public string CodeProbleme { get; private set; }
        public string LibelleProbleme { get; set; }
        #endregion

        /// <summary>
        /// Constructeur de la classe Probleme
        /// </summary>
        /// <param name="libelleProbleme"></param>
        public Probleme(string libelleProbleme)
        {
            this.LibelleProbleme = libelleProbleme;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql = "SELECT codeProbleme, libelleProbleme FROM PROBLEME";
        private static string _selectById = "SELECT codeProbleme, libelleProbleme FROM PROBLEME WHERE codeProbleme = ?codeProbleme";
        private static string _insertSql = "INSERT INTO PROBLEME (codeProbleme, libelleCodeProbleme) VALUES (?codeProbleme, ?libelleProbleme)";
        private static string _updateById = "UPDATE PROBLEME SET libelleProbleme = ?libelleProbleme WHERE codeProbleme = ?codeProbleme";
        private static string _deleteById = "DELETE FROM PROBLEME WHERE codeProbleme = ?codeProbleme";
        #endregion

        #region Méthode d'accès aux données
        /// <summary>
        /// Valorise un objet de type Probleme
        /// </summary>
        /// <param name="codeProbleme"></param>
        /// <returns></returns>
        public static Probleme Fetch(string codeProbleme)
        {
            Probleme unProbleme = null;
            string libelle;
            //Connexion à la base de données
            DataBaseAccess.Connexion.Open();

            //Initialisation d'un objet permettant d'interroger la base de donnéeds
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();

            //Requete à utiliser
            commandeSql.CommandText = _selectById;

            //Parametres à transmettre
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeProbleme", codeProbleme));

            //Préparation de la requète
            commandeSql.Prepare();

            //Execution de la requete
            IDataReader enregistrement = commandeSql.ExecuteReader();

            //Lecture du premier enregistrement
            bool existeEnregistrement = enregistrement.Read();

            if (existeEnregistrement)
            {
                libelle = enregistrement["libelleProbleme"].ToString();
                unProbleme = new Probleme(libelle);

            }
            DataBaseAccess.Connexion.Close();
            return unProbleme;
        }

        /// <summary>
        /// Retourne une collection de problème
        /// </summary>
        /// <returns></returns>
        public static List<Probleme> FetchAll()
        {

            List<Probleme> collectionProbleme = new List<Probleme>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _selectSql;
            IDataReader enregistrement = commandeSql.ExecuteReader();
            while (enregistrement.Read())
            {
                Probleme unProbleme = new Probleme(enregistrement["libelleProbleme"].ToString());
                string codeProbleme = enregistrement["codeProbleme"].ToString();
                unProbleme.CodeProbleme = codeProbleme;
                collectionProbleme.Add(unProbleme);
            }
            DataBaseAccess.Connexion.Close();
            return collectionProbleme;
        }

        /// <summary>
        /// Sauvegarde ou mise à jour d'un probleme
        /// </summary>
        public void Save()
        {
            if (true)
            {

            }
        }

        /// <summary>
        /// Supprime le problème courant
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _deleteById;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeProbleme", CodeProbleme));
            commandeSql.Prepare();
            int nbLignesAffectees = commandeSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Modifie le problème courant
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _updateById;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeProbleme", CodeProbleme));
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?libelleProbleme", LibelleProbleme));
            commandeSql.Prepare();
            commandeSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Inserer un problème
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _insertSql;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeProbleme", CodeProbleme));
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?libelleProbleme", LibelleProbleme));
            commandeSql.Prepare();
            commandeSql.ExecuteNonQuery();
            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParametre("?codeProbleme", CodeProbleme));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParametre("?libelleProbleme", LibelleProbleme));
            commandGetLastId.Prepare();
            IDataReader enregistrement = commandGetLastId.ExecuteReader();
            bool existeEnregistrement = enregistrement.Read();
            if (existeEnregistrement)
            {
                CodeProbleme = enregistrement["codeProbleme"].ToString();
            }
            else
            {
                commandeSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }
        #endregion
    }
}
