﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Reflection;


namespace Gestion_Container
{
    /// <summary>
    /// Classe Déclaration: Représentation d'une déclaration
    /// </summary>
    class Declaration
    {
        #region Modèle Objet
        public string CodeDeclaration { get; private set; }
        public string LibelleDeclaration { get; set; }
        public DateTime DateDeclaration { get; set; }
        public bool Validation { get; set; }
        public Probleme probleme { get; set; }
        public Container Container { get; set; }
        #endregion

        /// <summary>
        /// Constructeur de la classe Delcaration
        /// </summary>
        /// <param name="commentaire"></param>
        /// <param name="probleme"></param>
        /// <param name="urgence"></param>
        /// <param name="traite"></param>
        /// <param name="container"></param>
        public Declaration(string commentaire, Probleme codePr, bool urgence, bool validation, Container container)
        {
            this.CodeDeclaration = commentaire;
            this.DateDeclaration = DateTime.Now;
            this.Validation = validation;
            this.probleme = probleme;
            this.Container = container;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql = "SELECT codeDeclaration, libelleDeclaration, dateDeclaration, validation, codeProbleme, numContainer FROM DECLARATION";
        private static string _selectById = "SELECT codeDeclaration, libelleDeclaration, dateDeclaration, validation, codeProbleme, numContainer FROM DECLARATION WHERE codoDeclaration = ?codeDelcaration";
        private static string _insertSql = "INSERT INTO DELCARATION (libelleDeclaration, dateDeclaration, validation, codeProbleme, numContainer) VALUES (?libelleDeclaration, ?dateDeclaration, ?validation, ?codeProbleme, ?numContainer)";
        private static string _updateById = "UPDATE DELCARATION SET libelleDeclaration = ?libelleDeclaration, dateDeclaration = ?dateDeclaration, validation = ?validation, codeProbleme = ?codeProbleme, numContainer = ?numContainer WHERE codeDelcaration = ?codeDeclaration";
        private static string _deleteById = "DELETE FROM DECLRATION WHERE codeDeclaration = ?codeDeclaration";
        #endregion

        #region Méthode d'accès aux données
        /// <summary>
        /// Valorise un objet de type Declaration
        /// </summary>
        /// <param name="codeDeclaration">Valeur de la clé primaire</param>
        /// <returns></returns>
        public static Declaration Fetch(string codeDeclaration)
        {
            Declaration uneDeclaration = null;

            //Connexion à la base de données
            DataBaseAccess.Connexion.Open();

            //Initialisation d'un objet permettant d'interroger la base de donnéeds
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();

            //Requete à utiliser
            commandeSql.CommandText = _selectById;

            //Parametres à transmettre
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeDeclaration", codeDeclaration));

            //Préparation de la requète
            commandeSql.Prepare();

            //Execution de la requete
            IDataReader enregistrement = commandeSql.ExecuteReader();

            //Lecture du premier enregistrement
            bool existeEnregistrement = enregistrement.Read();
            return uneDeclaration;
        }


        #endregion
    }
}
