﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;
using MySql.Data.MySqlClient;

namespace Gestion_Container
{
    public partial class FenetrePrincipale : Form
    {
        Form MdiChild;
        
        public FenetrePrincipale()
        {
            InitializeComponent();
        }

        private void ButtonConnexion_Click(object sender, EventArgs e)
        {
            DirectoryEntry Ldap;
            try
            {
                //connexion à l'annuaire LDAP
                Ldap = new DirectoryEntry("LDAP://tholdi.com/OU=OU-Fret&Maintenance,OU=OU-Paris,DC=tholdi,DC=com", @"glpi", "Xazerty1");

                // l'objet DirectorySearcher permet d'effectuer une recherche
                DirectorySearcher searcher = new DirectorySearcher(Ldap);

                // on filtre pour n'afficher que les utilisateurs
                searcher.Filter = "(objectClass=user)";

                // pour chaque résultat trouvé
                foreach (SearchResult result2 in searcher.FindAll())
                {
                    
                    // On récupère l'objet trouvé lors de la recherche
                    DirectoryEntry DirEntry2 = result2.GetDirectoryEntry();
                }
                // on affiche l'info souhaitée dans une liste déroulante
                //lst_users.Items.Add("mail : " + DirEntry.Properties["SAMAccountName"].Value + " nom : " + DirEntry.Properties["sn"].Value);
                // Nouvel objet pour instancier la recherche
                DirectorySearcher searcher2 = new DirectorySearcher(Ldap);

                    // On modifie le filtre pour ne chercher que l'user dont le nom de login est celui de David ANDRE
                    searcher2.Filter = "(saMAccountName=" + textBoxLogin.Text + ")";

                    // Pas de boucle foreach car on ne cherche qu'un user
                    SearchResult result = searcher2.FindOne();

                // on récupère l'entrée trouvée lors de la recherche
                DirectoryEntry DirEntry = result.GetDirectoryEntry();

                // On vérifie que son nom correspond à ce qu'il y a dans la saisie
                if (string.Compare(DirEntry.Properties["sn"].Value.ToString(), textBoxMotDePasse.Text) == 0)
                    {
                        MessageBox.Show("Connexion établie");
                        MdiChild = new AccueilChef();
                        MdiChild.Show();
                    }
                    else
                        MessageBox.Show("erreur");


                

            }
            catch (Exception Ex)
            {
                MessageBox.Show("erreur LDAP " + Ex.Message);

            }
        }

        private void FenetrePrincipale_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        
    }
}