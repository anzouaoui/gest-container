﻿Create table MOTIF(
codeMotif char(4),
libelleMotif varchar(30) not null,
constraint pk_motif primary key (codeMotif)
)
engine=innodb;

Create table ETAT(
codeEtat int,
libelleEtat varchar(30)not null,
constraint pk_etat primary Key (codeEtat)
)
engine=innodb;

create table TRAVAUX(
codeTravaux char(6),
libelleTravaux varchar(30) not null,
dureeImmobilisation int(4) not null,
constraint pk_travaux primary key (codeTravaux)
)
engine=innodb;

create table PROBLEME(
codeProbleme int auto_increment,
libelleProbleme varchar(50) not null,
delai int(4),
Constraint pk_probleme primary key (codeProbleme)
)
engine=innodb;

create table CONTAINER(
numContainer int auto_increment,
dateAchat date,
typeContainer varchar(30)not null,
dateLimite date,
dateInspection date,
Constraint pk_container primary key (numContainer)
)
engine=innodb;

create table DECLARATION(
codeDeclaration int auto_increment,
libelleDeclaration varchar(50) not null,
validation bool default false,
codeProbleme int,
numContainer int,
Constraint pk_declaration primary key (codeDeclaration),
Constraint fk_probleme foreign key (codeProbleme) references PROBLEME (codeProbleme),
Constraint fk_container foreign key (numContainer) references CONTAINER (numContainer)
)
engine=innodb;

Create table INSPECTION(
numInspection int,
numContainer int,
dateInspection date,
codeMotif char(4) not null,
codeEtat int default 1,
Constraint pk_inspection primary key (numInspection, numContainer),
Constraint fk_motif foreign key (codeMotif) references MOTIF (codeMotif),
Constraint fk_etat foreign key (codeEtat) references ETAT (codeEtat)
)
engine=innodb;

create table DECISION(
numInspection int,
codeTravaux char(6),
dateEnvoi date,
dateRetour date,
commentaire varchar(100),
cout decimal(6.2),
constraint pk_decision primary key (numInspection, codeTravaux),
constraint fk_inspection foreign key (numInspection) references INSPECTION (numInspection),
constraint fk_travaux foreign key (codeTravaux) references TRAVAUX (codeTravaux)
)
engine=innodb;