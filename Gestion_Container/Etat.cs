﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace Gestion_Container
{
    class Etat
    {
        #region Modèle d'objet
        private int _codeEtat;
        private string _libelleEtat;
        #endregion

        // Ascesseurs de la classe Etat :
        // Pas de mutateurs car les états sont définis !!

        public int CodeEtat
        {
            get { return _codeEtat; }
            set { _codeEtat = value; }
        }

        public string LibelleEtat
        {
            get { return _libelleEtat; }
            set { _libelleEtat = value; }
        }
        
        /// <summary>
        /// Constructeur de la classe Etat
        /// </summary>
        /// <param name="codeEtat"></param>
        /// <param name="libelleEtat"></param>
        public Etat(string libelleEtat)
        {
            this.LibelleEtat = libelleEtat;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données
        private static string _selectSql = "SELECT codeEtat, libelleEtat FROM ETAT";
        private static string _selectById = "SELECT odeEtat, libelleEtat FROM ETAT WHERE codeEtat = ?codeEtat";
        private static string _insertSql = "INSERT INTO ETAT (codeEtat, libelleEtat) VALUES (?codeEtat, ?libelleEtat)";
        private static string _updateById = "UPDATE ETAT SET libelleEtat = ?libelleEtat WHERE codeEtat = ?codeEtat";
        private static string _deleteById = "DELETE FROM ETAT WHERE codeEtat = ?codeEtat";
        #endregion

        #region Méthode d'accès aux données
        /// <summary>
        /// Valorise un objet de type Etat
        /// </summary>
        /// <param name="codeEtat"></param>
        /// <returns>Etat unEtat</returns>
        public static Etat Fetch(string codeEtat)
        {
            Etat unEtat = null;
            string libelle;
            //Connexion à la base de données
            DataBaseAccess.Connexion.Open();

            //Initialisation d'un objet permettant d'interroger la base de donnéeds
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();

            //Requete à utiliser
            commandeSql.CommandText = _selectById;

            //Parametres à transmettre
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeEtat", codeEtat));

            //Préparation de la requète
            commandeSql.Prepare();

            //Execution de la requete
            IDataReader enregistrement = commandeSql.ExecuteReader();

            //Lecture du premier enregistrement
            bool existeEnregistrement = enregistrement.Read();

            if (existeEnregistrement)
            {
                libelle = enregistrement["libelleEtat"].ToString();
                unEtat = new Etat(libelle);

            }
            DataBaseAccess.Connexion.Close();
            return unEtat;
        }

        /// <summary>
        /// Retourne une collection d'état
        /// </summary>
        /// <returns></returns>
        public static List<Etat> FetchAll()
        {

            List<Etat> collectionEtat = new List<Etat>();
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _selectSql;
            IDataReader enregistrement = commandeSql.ExecuteReader();
            while (enregistrement.Read())
            {
                Etat unEtat = new Etat(enregistrement["libelleEtat"].ToString());
                string codeEtat = enregistrement["codeEtat"].ToString();
                unEtat.CodeEtat = Convert.ToInt32(codeEtat);
                collectionEtat.Add(unEtat);
            }
            DataBaseAccess.Connexion.Close();
            return collectionEtat;
        }

        /// <summary>
        /// Supprime le problème courant
        /// </summary>
        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _deleteById;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeEtat", CodeEtat));
            commandeSql.Prepare();
            int nbLignesAffectees = commandeSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Modifie le problème courant
        /// </summary>
        private void Update()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _updateById;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeEtat", CodeEtat));
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?libelleEtat", LibelleEtat));
            commandeSql.Prepare();
            commandeSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        /// <summary>
        /// Inserer un problème
        /// </summary>
        private void Insert()
        {
            DataBaseAccess.Connexion.Open();
            IDbCommand commandeSql = DataBaseAccess.Connexion.CreateCommand();
            commandeSql.CommandText = _insertSql;
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?codeEtat", CodeEtat));
            commandeSql.Parameters.Add(DataBaseAccess.CodeParametre("?libelleEtat", LibelleEtat));
            commandeSql.Prepare();
            commandeSql.ExecuteNonQuery();
            IDbCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParametre("?codeEtat", CodeEtat));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParametre("?libelleEtat", LibelleEtat));
            commandGetLastId.Prepare();
            IDataReader enregistrement = commandGetLastId.ExecuteReader();
            bool existeEnregistrement = enregistrement.Read();
            if (existeEnregistrement)
            {
                CodeEtat = Convert.ToInt32(enregistrement["codeEtat"].ToString());
            }
            else
            {
                commandeSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }
        #endregion
    }

}
