﻿namespace Gestion_Container
{
    partial class AccueilDocker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.iNSPECTIONSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archivesDesInspectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectionsPrévusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pROBLEMESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Teal;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNSPECTIONSToolStripMenuItem,
            this.pROBLEMESToolStripMenuItem,
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(684, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // iNSPECTIONSToolStripMenuItem
            // 
            this.iNSPECTIONSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consulterToolStripMenuItem,
            this.archivesDesInspectionsToolStripMenuItem,
            this.inspectionsPrévusToolStripMenuItem});
            this.iNSPECTIONSToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.iNSPECTIONSToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.iNSPECTIONSToolStripMenuItem.Name = "iNSPECTIONSToolStripMenuItem";
            this.iNSPECTIONSToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.iNSPECTIONSToolStripMenuItem.Text = "INSPECTIONS";
            // 
            // consulterToolStripMenuItem
            // 
            this.consulterToolStripMenuItem.Name = "consulterToolStripMenuItem";
            this.consulterToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.consulterToolStripMenuItem.Text = "Inspections en Cours";
            // 
            // archivesDesInspectionsToolStripMenuItem
            // 
            this.archivesDesInspectionsToolStripMenuItem.Name = "archivesDesInspectionsToolStripMenuItem";
            this.archivesDesInspectionsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.archivesDesInspectionsToolStripMenuItem.Text = "Archives des Inspections";
            // 
            // inspectionsPrévusToolStripMenuItem
            // 
            this.inspectionsPrévusToolStripMenuItem.Name = "inspectionsPrévusToolStripMenuItem";
            this.inspectionsPrévusToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.inspectionsPrévusToolStripMenuItem.Text = "Inspections Prévus";
            // 
            // pROBLEMESToolStripMenuItem
            // 
            this.pROBLEMESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consulterToolStripMenuItem1});
            this.pROBLEMESToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.pROBLEMESToolStripMenuItem.Name = "pROBLEMESToolStripMenuItem";
            this.pROBLEMESToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.pROBLEMESToolStripMenuItem.Text = "PROBLEMES";
            // 
            // consulterToolStripMenuItem1
            // 
            this.consulterToolStripMenuItem1.Name = "consulterToolStripMenuItem1";
            this.consulterToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.consulterToolStripMenuItem1.Text = "Signaler un Problèmes";
            this.consulterToolStripMenuItem1.Click += new System.EventHandler(this.consulterToolStripMenuItem1_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(103, 20);
            this.toolStripMenuItem1.Text = "                            ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gabriola", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(153, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(379, 50);
            this.label1.TabIndex = 3;
            this.label1.Text = "Bonjour et Bienvenue sur l\'Application";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gestion_Container.Properties.Resources.minilogo;
            this.pictureBox1.Location = new System.Drawing.Point(162, 143);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(348, 127);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // AccueilDocker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(684, 361);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "AccueilDocker";
            this.Text = "Accueil";
            this.Load += new System.EventHandler(this.AccueilDocker_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem iNSPECTIONSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pROBLEMESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem archivesDesInspectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inspectionsPrévusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}