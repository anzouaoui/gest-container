﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Container
{
    class Container
    {
        private int _numContainer;
        private DateTime _dateAchat;
        private DateTime _dateLimite;
        private String _typeContainer;

        public int NumContainer
        {
            get { return _numContainer; }
        }
        public DateTime DateAchat
        {
            get { return _dateAchat; }
        }
        public DateTime DateLimite
        {
            get { return _dateLimite; }
        }
        public string TypeContainer
        {
            get { return _typeContainer; }
            set { _typeContainer = value; }
        }

        public Container(int numContainer, DateTime dateAchat, DateTime DateLimite, string typeContainer)
        {
            _numContainer = numContainer;
            _dateAchat = dateAchat;
            _dateLimite = DateLimite;
            _typeContainer = typeContainer;

        }

        //Methode de la classe
    }

}
