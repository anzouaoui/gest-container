﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestion_Container
{
    class Travaux
    {
        public string codeTravaux { get; private set; }
        public string libelleTravaux { get; set; }
        public int dureeImmobilisation { get; set; }

        public Travaux(string code, string libelle, int duree)
        {
            codeTravaux = code;
            libelleTravaux = libelle;
            dureeImmobilisation = duree;
        }
    }
}
