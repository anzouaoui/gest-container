﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace Gestion_Container
{
    public partial class AccueilChef : Form
    {
        public AccueilChef()
        {
            InitializeComponent();
        }

        private void AccueilChef_Load(object sender, EventArgs e)
        {

        }

        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        private void saisirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Form().Show();
        }
    }
}
