﻿namespace Gestion_Container
{
    partial class AccueilChef
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.iNSPECTIONSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saisirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.archiveDesInspectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inspectionPrévusToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pROBLEMESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consulterToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.archivesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Teal;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iNSPECTIONSToolStripMenuItem,
            this.pROBLEMESToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(696, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // iNSPECTIONSToolStripMenuItem
            // 
            this.iNSPECTIONSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saisirToolStripMenuItem,
            this.consulterToolStripMenuItem,
            this.archiveDesInspectionsToolStripMenuItem,
            this.inspectionPrévusToolStripMenuItem});
            this.iNSPECTIONSToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.iNSPECTIONSToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.White;
            this.iNSPECTIONSToolStripMenuItem.Name = "iNSPECTIONSToolStripMenuItem";
            this.iNSPECTIONSToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.iNSPECTIONSToolStripMenuItem.Text = "INSPECTIONS";
            // 
            // saisirToolStripMenuItem
            // 
            this.saisirToolStripMenuItem.Name = "saisirToolStripMenuItem";
            this.saisirToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.saisirToolStripMenuItem.Text = "Inspections à Valider";
            this.saisirToolStripMenuItem.Click += new System.EventHandler(this.saisirToolStripMenuItem_Click);
            // 
            // consulterToolStripMenuItem
            // 
            this.consulterToolStripMenuItem.Name = "consulterToolStripMenuItem";
            this.consulterToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.consulterToolStripMenuItem.Text = "Inspections en Cours";
            // 
            // archiveDesInspectionsToolStripMenuItem
            // 
            this.archiveDesInspectionsToolStripMenuItem.Name = "archiveDesInspectionsToolStripMenuItem";
            this.archiveDesInspectionsToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.archiveDesInspectionsToolStripMenuItem.Text = "Archives des Inspections";
            // 
            // inspectionPrévusToolStripMenuItem
            // 
            this.inspectionPrévusToolStripMenuItem.Name = "inspectionPrévusToolStripMenuItem";
            this.inspectionPrévusToolStripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.inspectionPrévusToolStripMenuItem.Text = "Inspections Prévus";
            // 
            // pROBLEMESToolStripMenuItem
            // 
            this.pROBLEMESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.consulterToolStripMenuItem1,
            this.archivesToolStripMenuItem});
            this.pROBLEMESToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.pROBLEMESToolStripMenuItem.Name = "pROBLEMESToolStripMenuItem";
            this.pROBLEMESToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
            this.pROBLEMESToolStripMenuItem.Text = "PROBLEMES";
            // 
            // consulterToolStripMenuItem1
            // 
            this.consulterToolStripMenuItem1.Name = "consulterToolStripMenuItem1";
            this.consulterToolStripMenuItem1.Size = new System.Drawing.Size(176, 22);
            this.consulterToolStripMenuItem1.Text = "Problèmes Signalés";
            // 
            // archivesToolStripMenuItem
            // 
            this.archivesToolStripMenuItem.Name = "archivesToolStripMenuItem";
            this.archivesToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.archivesToolStripMenuItem.Text = "Archives";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gabriola", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(160, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(379, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "Bonjour et Bienvenue sur l\'Application";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gestion_Container.Properties.Resources.minilogo;
            this.pictureBox1.Location = new System.Drawing.Point(169, 144);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(348, 127);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // AccueilChef
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(696, 384);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "AccueilChef";
            this.Text = "Accueil";
            this.Load += new System.EventHandler(this.AccueilChef_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem iNSPECTIONSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saisirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pROBLEMESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolStripMenuItem consulterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archiveDesInspectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inspectionPrévusToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem archivesToolStripMenuItem;
    }
}