﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.DirectoryServices;

namespace Gestion_Container
{
    public partial class ConsulterProblème : Form
    {
        private List<Probleme> _collectionProbleme = new List<Probleme>();
        public ConsulterProblème()
        {
            InitializeComponent();
        }

        private void ConsulterProblème_Load(object sender, EventArgs e)
        {
            recupererProbleme();
        }

        private void recupererProbleme()
        {
            _collectionProbleme = Probleme.FetchAll();
            dataGridViewprobleme.Rows.Clear();
            foreach (Probleme problemeCourant in _collectionProbleme)
            {
                dataGridViewprobleme.Rows.Add(problemeCourant.CodeProbleme, problemeCourant.LibelleProbleme);
            }
        }
    }
}
