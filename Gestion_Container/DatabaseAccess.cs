﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;

namespace Gestion_Container
{
    class DataBaseAccess
    {
        /// <summary>
        /// Variable de connexion à la base de données
        /// </summary>
        public static MySqlConnection Connexion = new MySqlConnection("Database = gestContainer ; Data Source = 127.0.0.1; UID = root; Password =");
        //public static MySqlConnection Connexion = new MySqlConnection("Database = gestContainer ; Data Source = 192.168.0.20; UID = zouaoui; Password = 12/05/1996");

        public static MySqlParameter CodeParametre(string paramName, object value)
        {
            MySqlCommand commandeSql = Connexion.CreateCommand();
            MySqlParameter parametre = commandeSql.CreateParameter();
            parametre.ParameterName = paramName;
            return parametre;
        }
    }
}
