﻿namespace Gestion_Container
{
    partial class ConsulterProblème
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewprobleme = new System.Windows.Forms.DataGridView();
            this.CodeProbleme = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LibelleProbleme = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewprobleme)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewprobleme
            // 
            this.dataGridViewprobleme.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridViewprobleme.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dataGridViewprobleme.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridViewprobleme.ColumnHeadersHeight = 32;
            this.dataGridViewprobleme.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CodeProbleme,
            this.LibelleProbleme});
            this.dataGridViewprobleme.Cursor = System.Windows.Forms.Cursors.Default;
            this.dataGridViewprobleme.Location = new System.Drawing.Point(53, 95);
            this.dataGridViewprobleme.Name = "dataGridViewprobleme";
            this.dataGridViewprobleme.RowHeadersWidth = 120;
            this.dataGridViewprobleme.Size = new System.Drawing.Size(394, 268);
            this.dataGridViewprobleme.TabIndex = 0;
            // 
            // CodeProbleme
            // 
            this.CodeProbleme.HeaderText = "Code Probleme";
            this.CodeProbleme.Name = "CodeProbleme";
            this.CodeProbleme.Width = 96;
            // 
            // LibelleProbleme
            // 
            this.LibelleProbleme.HeaderText = "Libelle";
            this.LibelleProbleme.Name = "LibelleProbleme";
            this.LibelleProbleme.Width = 62;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Gabriola", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "Liste des Problèmes signalés :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Gestion_Container.Properties.Resources.minilogo;
            this.pictureBox1.Location = new System.Drawing.Point(563, 42);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(90, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 24;
            this.pictureBox1.TabStop = false;
            // 
            // ConsulterProblème
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(707, 398);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewprobleme);
            this.Name = "ConsulterProblème";
            this.Text = "Consulter Problème";
            this.Load += new System.EventHandler(this.ConsulterProblème_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewprobleme)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewprobleme;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CodeProbleme;
        private System.Windows.Forms.DataGridViewTextBoxColumn LibelleProbleme;
    }
}